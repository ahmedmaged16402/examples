#include <stdio.h>
#include <pthread.h>

void* hello_func(void* arg) {
	printf("Hello Thread\n");

	return 0;
}

int main(int argc, char** argv) {
	pthread_t thread;

	pthread_create(&thread, NULL, hello_func, NULL);

	/* Wait until all threads have terminated. */
	pthread_join(thread, NULL);

	return 0;
}
