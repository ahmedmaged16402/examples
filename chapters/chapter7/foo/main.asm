DEFAULT REL

SECTION .data
	msg db "j = %d", 10, 13, 0

SECTION .text

; define main as globale, which is recognizable by the linker  
global main
; define printf as external symbol
extern printf

foo:
	; create new stack frame
	push rbp
	mov rbp, rsp

	; create space for the local variable j
	sub rsp, 16

	; int j = x;
	mov [rsp], rdi

	; printf("j = %d\n", j);
	mov rdi, msg
	mov rsi, [rsp]
	call printf

	; remove local variable j
	add rsp, 16

	; restore rbp
	pop rbp

	ret

main:
	; create new stack frame
	push rbp
	mov rbp, rsp

	; dump register states
	mov rdi, 42
	call foo

	; restore rbp
	pop rbp

	; Leave program & signalize, that no error occurred during the execution
	mov rax, 0
	ret

%ifidn __OUTPUT_FORMAT__,elf64
section .note.GNU-stack noalloc noexec nowrite progbits
%endif
