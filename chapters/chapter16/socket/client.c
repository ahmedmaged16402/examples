#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

static char *socket_path = "./gin3-socket";

int main(int argc, char** argv) {
  struct sockaddr_un addr;
  char msg[] = "Hello World!";
  int fd, rc, i;

  if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("socket error");
    exit(EXIT_FAILURE);
  }
  
  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);

  if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    perror("connect error");
    exit(EXIT_FAILURE);
  }

  i = 0;
  while(i < strlen(msg)) {
    rc = write(fd, msg+i, strlen(msg)-i);
    if (rc >= 0) {
      i += rc;
    } else {
      perror("write error");
      exit(EXIT_FAILURE);
    }
  }

  return 0;
}
