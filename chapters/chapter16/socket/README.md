# Unix Domain Sockets

This example shows the inter-process communication by UNIX domain sockets.

* Compile demo on Linux and macOS: make
* Run demo: make test

Output on a Linux system:

```
./server &
sleep 1
./client
read 12 bytes: Hello World!
```
