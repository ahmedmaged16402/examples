#include <stdalign.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

alignas(16) double step;
alignas(16) double sum;
alignas(16) long long num_steps = 1000000;

alignas(16) double four[] = {4.0, 4.0};
alignas(16) double two[] = {2.0, 2.0};
alignas(16) double one[] = {1.0, 1.0};
alignas(16) double ofs[] = {0.5, 1.5};

int hasSSE2(void);
void calcPi_SSE(void);

int main(int argc, char **argv)
{
	struct timeval start, end;

	if (argc > 1)
		num_steps = atoll(argv[1]);
	if (num_steps < 100)
		num_steps = 1000000;
	printf("num_steps = %lld\n", num_steps);

	if (hasSSE2()) {
		gettimeofday(&start, NULL);

		sum = 0.0;
		step = 1.0 / (double)num_steps;

		calcPi_SSE();

		gettimeofday(&end, NULL);
		printf("PI = %f (SSE)\n", sum * step);
		printf("Time : %lf sec (SSE)\n", (double)(end.tv_sec-start.tv_sec)+(double)(end.tv_usec-start.tv_usec)/1000000.0);
	} else {
		printf("System doesn't support SSE2!\n");
	}

	return 0;
}
